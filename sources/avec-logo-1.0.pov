// POV-Ray 3.5 scene file by Dan Farmer

#include "colors.inc"
#include "woods.inc"

//-----------------------
#declare Texture =
	texture
      	{
      	T_Wood4
      	finish { specular 0.25 roughness 0.015 ambient 0.5 }
       	}
#include "avec-logo-core-1.0.inc"

global_settings {max_trace_level 5}

/********************************************************
	CAMERA
********************************************************/
camera
	{
	location <0, 8,0>
	look_at <0,0,0>
	}

/********************************************************
	SOURCES DE LUMIERES
********************************************************/
#declare Dist=80.0;
//light_source {< 0, 20, 0> color rgb<244,225,225>}
light_source {< 0, 20, 0> color White}


/********************************************************
	MAIN
********************************************************/
object { avec_logo 
	rotate 120*y*clock
	texture
		{
//		T_Wood4
		//T_Wood9	finish { specular 0.25 roughness 0.015 ambient 0.5 }
    	T_Wood4
		pigment	{turbulence 2}
 //   	normal	{}
    	finish {  specular 0.25 roughness 0.015 ambient 0.5 }		
		
		}
	}

