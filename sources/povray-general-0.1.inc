/*
// file: povray-general-0.1.inc
// date:
// auteur: pascal TOLEDO
// site: http://www.legral.fr
// description: scene exemple avec details exhautives de toutes les directives
*/

/********************************************************
	REPERES
********************************************************/

//------------------------------ the Axes --------------------------------
#macro Axis_( AxisLen, Dark_Texture,Light_Texture) 
 union{
    cylinder { <0,-AxisLen,0>,<0,AxisLen,0>,0.05   
               texture{checker texture{Dark_Texture } 
                               texture{Light_Texture}
                       translate<0.1,0,0.1>}
             }
    cone{<0,AxisLen,0>,0.2,<0,AxisLen+0.7,0>,0
          texture{Dark_Texture}
         }
     } // end of union                   
#end // of macro "Axis()"

//------------------------------------------------------------------------
#macro AxisXYZ( AxisLenX, AxisLenY, AxisLenZ, Tex_Dark, Tex_Light)
//--------------------- drawing of 3 Axes --------------------------------
union{
	#if (AxisLenX != 0)
		object { Axis_(AxisLenX, Tex_Dark, Tex_Light)   rotate< 0,0,-90>}// x-Axis
		text   { ttf "timrom.ttf",  "x",  0.15,  0  texture{Tex_Dark} rotate<30,-40,0> scale 0.75 translate <AxisLenX+0.05,0.4, 0.00> no_shadow}
	#end // of #if 

	#if (AxisLenY != 0)
		object { Axis_(AxisLenY, Tex_Dark, Tex_Light)   rotate< 0,0,  0>}// y-Axis
		text   { ttf "timrom.ttf",  "y",  0.15,  0  texture{Tex_Dark} scale 0.5 translate <-0.75,AxisLenY+0.10,-0.10>  no_shadow}
	#end // of #if 

	#if (AxisLenZ != 0)
		object { Axis_(AxisLenZ, Tex_Dark, Tex_Light)   rotate<90,0,  0>}// z-Axis
		text   { ttf "timrom.ttf",  "z",  0.15,  0  texture{Tex_Dark} rotate<30,-40,0> scale 0.75 translate <-0.45,0.1,AxisLenZ+0.00>  no_shadow}
	#end // of #if 
	} // end of union
#end// of macro "AxisXYZ( ... )"
//------------------------------------------------------------------------
#declare Texture_A_Dark  = texture {
                               pigment{ color rgb<1,0.45,0>}
                               finish { phong 1}
                             }
#declare Texture_A_Light = texture { 
                               pigment{ color rgb<1,1,1>}
                               finish { phong 1}
                             }

/*
Exemple:
object{ AxisXYZ( 3.5, 9.25, 7, Texture_A_Dark, Texture_A_Light)}
*/
//-------------------------------------------------- end of coordinate axes

