/*
// file: avec-logo-1.0.inc
// auteur: pascal TOLEDO
// site: http://www.legral
// description: logo de l'association avec


// exemple de texture
#include "colors.inc"
#include "woods.inc"

#declare avec_Texture_defaut =
	texture
      	{
      	T_Wood4
      	finish { specular 0.25 roughness 0.015 ambient 0.5 }
       	}
*/


#declare nombreOr=1.618033989;

#declare avec_logo_idiome_longueur=2.5;
#declare avec_logo_idiome_largeur=1;

#declare avec_logo_idiome_hauteur=avec_logo_idiome_largeur/2;
#declare avec_logo_idiome_longueur_corps=avec_logo_idiome_longueur/nombreOr;
#declare avec_logo_idiome_longueur_bras=avec_logo_idiome_longueur-avec_logo_idiome_longueur_corps;

     

#declare avec_logo_idiome_rayon=avec_logo_idiome_largeur/2;

//************************************************************
#declare avec_logo_idiome_Tete =
	cylinder
		{
		<0,0,0>, <0,avec_logo_idiome_hauteur,0>, avec_logo_idiome_rayon
		}

#declare avec_logo_idiome_Corps =
	union
	{
	cylinder
		{<0,0,0>, <0,avec_logo_idiome_hauteur,0>,avec_logo_idiome_rayon
		}

     box
     	{<0,0,0>, <avec_logo_idiome_longueur_corps,avec_logo_idiome_hauteur,1>
		translate -avec_logo_idiome_rayon*z
     	} 
	}
	
#declare avec_logo_idiome_Bras =
	union
	{
	cylinder
		{<0,0,0>, <0,avec_logo_idiome_hauteur,0>,avec_logo_idiome_rayon
		}

     box
     	{<0,0,0>, <avec_logo_idiome_longueur_bras,avec_logo_idiome_hauteur,1>
		//texture{Texture}
		translate -avec_logo_idiome_rayon*z
     	} 
	}


#declare avec_logo_idiome=union
	{
	object { avec_logo_idiome_Tete }
	object { avec_logo_idiome_Corps translate avec_logo_idiome_largeur*x}
	object { avec_logo_idiome_Bras  rotate y*60 translate avec_logo_idiome_largeur*x}
	
	translate <-(avec_logo_idiome_longueur/2),0,-avec_logo_idiome_rayon> 
	}


//**   AVEC-LOGO   **********************************************************

#declare avec_logo_dc=avec_logo_idiome_longueur-avec_logo_idiome_largeur;

#declare avec_logo=
union
	{
	object { avec_logo_idiome translate <-avec_logo_dc,0,avec_logo_dc> rotate y*000}
	object { avec_logo_idiome translate <-avec_logo_dc,0,avec_logo_dc> rotate y*120}
	object { avec_logo_idiome translate <-avec_logo_dc,0,avec_logo_dc> rotate y*240}
	//texture{avec_Texture_defaut}
	}


// *   MAIN   **********************************************************
// exemple d'utilisation
//object { avec_logo rotate 60*y texture{avec_Texture_defaut}}

