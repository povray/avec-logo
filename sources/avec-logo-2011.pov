/*
// file: avec-logo-2011
// auteur: pascal TOLEDO
// site: http://www.legral
// description: logo de l'association A.V.E.C. pour l'annee 2011
height=100;
width=100;

*/


#include "colors.inc"
#include "woods.inc"

/********************************************************
	PARAMETRES GLOBAUX
********************************************************/
global_settings {max_trace_level 5}


/********************************************************
	OBJET AVEC_LOGO
********************************************************/
#include "avec-logo-core-1.0.inc"

// ---- texture ---------------------------------------
#declare avec_logo_texture2011 =
texture
    	{
    	T_Wood4
    	finish { specular 0.25 roughness 0.015 ambient 0.5 }
	}


/********************************************************
	CAMERA
********************************************************/
camera
	{
	location <0,7,-0.60>
	look_at  <0,0,-0.60>
	}

/********************************************************
	REPERE
********************************************************/
#include "../../../lib/perso/povray-general/povray-general-0.1.inc"
#AxisXYZ( 1, 1, 1, Texture_A_Dark, Texture_A_Light)

/********************************************************
	SOURCES DE LUMIERES
********************************************************/
//#declare Dist=80.0;
light_source {< 0, 7, 0> color White}


/********************************************************
	MAIN
********************************************************/
object {avec_logo texture{avec_logo_texture2011}}

